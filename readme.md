# Goblin Gen

Simple generator for pathfinder goblins. Returns a http response

## Starting

To run from commandline:

to build container: `docker build . -t goblin-gen`

to run container: `docker run -p7000:7000 goblin-gen`

The page is then accessible from `localhost:7000`

## Requirements

* There are multiple types of goblin
* The exact same goblin is generated if the same seed is passed