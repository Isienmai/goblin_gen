FROM python:3.7-alpine

RUN apk update &&\
    apk add sassc --no-cache
RUN pip3 install pipenv

COPY Pipfile .
COPY Pipfile.lock .
RUN pipenv update

EXPOSE 7000/tcp

CMD pipenv run gunicorn --bind "0.0.0.0:7000" goblin_gen --access-logfile '-'

COPY goblin_gen goblin_gen
COPY tests tests

RUN sassc goblin_gen/style.sass goblin_gen/style.css

RUN pipenv run pytest tests --disable-warnings