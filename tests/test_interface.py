import sys

import random

import goblin_gen
from bs4 import BeautifulSoup


def get_app():
    goblin_gen.application.testing = True
    return goblin_gen.application.test_client()


def test_root_response():
    app = get_app()
    response = app.get("/")
    assert response.status_code == 307  # Redirect without cache


def test_response_with_seed():
    app = get_app()
    for x in range(50):
        response = app.get(f"/{x}")
        assert response.status_code == 200, response


def test_page_has_elements():
    seed = random.randint(0, 999)
    app = get_app()
    response = app.get(f"/{seed}")
    soup = BeautifulSoup(response.data, "html.parser")

    assert soup.find("title")
    
    assert soup.find(id="abilities")
    assert soup.find(id="defence")
    assert soup.find(id="offense")
    assert soup.find(id="type")
