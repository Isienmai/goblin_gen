import random

from goblin_gen import Goblin
from goblin_gen.goblin import GoblinTypes
from goblin_gen import special_abilities


def test_generating_a_shit_tonne_of_gobbos():
    """Just generate some and check basics"""
    for x in range(1000):
        new_goblin = Goblin.random_goblin(x)
        assert new_goblin.str
        assert new_goblin.dex
        assert new_goblin.con
        assert new_goblin.int
        assert new_goblin.wis
        assert new_goblin.cha

        assert len(new_goblin.languages) >= 1

        assert new_goblin.get_ac() is not None

        assert new_goblin.height_inches
        assert new_goblin.gender in ["Male", "Female"]
        assert new_goblin.weight
        assert new_goblin.hair_colour
        assert new_goblin.eyes

        assert new_goblin.special_abilities is not None


def test_benchmark_goblin_generation(benchmark):
    benchmark(
        lambda: Goblin.random_goblin(random.randint(0, 9999)))


def test_language():
    """Check intelligent goblins have multiple languages"""
    for x in range(20):
        languages = Goblin.Gen.languages(x)
        amount = max(Goblin.get_modifier(x) + 1, 1)
        assert len(languages) == amount


def test_same_seed_same_goblin():
    for x in range(50):
        one = Goblin.random_goblin(x)
        two = Goblin.random_goblin(x)

        assert one == two


def test_different_seed_different_goblin():
    for x in range(50):
        one = Goblin.random_goblin(x)
        two = Goblin.random_goblin(x + 1)

        assert one != two


def test_add_special_ability():
    goblin = GoblinTypes.normal_goblin().complete_stats()
    assert goblin.special_abilities == special_abilities.basic

    # Try add same ability
    goblin.add_special_ability(special_abilities.dark_vision)
    assert goblin.special_abilities == special_abilities.basic

    # Add different
    goblin.add_special_ability(special_abilities.light_blindness)
    assert goblin.special_abilities == \
           special_abilities.basic | {special_abilities.light_blindness}


def test_remove_special_ability():
    goblin = GoblinTypes.normal_goblin().complete_stats()
    assert goblin.special_abilities == special_abilities.basic

    goblin.remove_special_ability(special_abilities.dark_vision)
    assert goblin.special_abilities == {special_abilities.skilled, special_abilities.improved_initiative}


def test_thorny_reduces_dex():
    goblin = GoblinTypes.normal_goblin().complete_stats()
    old_dex = goblin.dex
    goblin.add_special_ability(special_abilities.thorny)
    assert goblin.dex == old_dex - 2


def test_default_hp():
    goblin = Goblin(str=11, dex=15, con=12, int=10, wis=9, cha=6)
    assert goblin.get_hp() == 6


def test_default_bab():
    goblin = Goblin(str=11, dex=15, con=12, int=10, wis=9, cha=6)
    assert goblin.get_bab() == 1


def test_default_ac_correct():
    goblin = Goblin(str=11, dex=15, con=12, int=10, wis=9, cha=6)
    assert goblin.get_ac() == 13
    assert goblin.get_touch_ac() == 13
    assert goblin.get_flat_footed_ac() == 11


def test_default_saves_correct():
    goblin = Goblin(str=11, dex=15, con=12, int=10, wis=9, cha=6)
    assert goblin.get_fort_save() == 3
    assert goblin.get_ref_save() == 2
    assert goblin.get_will_save() == -1


def test_default_CMB_CMD():
    goblin = Goblin(str=11, dex=15, con=12, int=10, wis=9, cha=6)
    assert goblin.get_cmb() == 0
    assert goblin.get_cmd() == 12


def test_default_carry_weights():
    goblin = Goblin(str=11, dex=15, con=12, int=10, wis=9, cha=6)
    assert goblin.get_light_load() == 28
    assert goblin.get_medium_load() == 57
    assert goblin.get_heavy_load() == 86


def test_init():
    for x in range(10, 20):
        goblin = GoblinTypes.normal_goblin()
        goblin.dex = x
        assert Goblin.get_modifier(x) + 4 == goblin.get_init()
