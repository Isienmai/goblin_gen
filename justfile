_:
    @just -l

upload:
    docker build . -t goblin-gen
    docker save -o goblin-gen.tar goblin-gen
    zip goblin-gen.zip goblin-gen.tar
    rsync goblin-gen.zip timmaddison.co.uk:/tmp/goblin-gen.zip --progress

    ssh -t timmaddison.co.uk unzip -o /tmp/goblin-gen.zip
    ssh -t timmaddison.co.uk docker stop goblin-gen || true
    ssh -t timmaddison.co.uk docker rename goblin-gen goblin-gen=old || true
    ssh -t timmaddison.co.uk docker load -i goblin-gen.tar
    ssh -t timmaddison.co.uk docker run -p7000:7000 -d --name goblin-gen goblin-gen
