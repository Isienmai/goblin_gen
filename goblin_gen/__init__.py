import sys
import random
import re

from flask import Flask, redirect, render_template, send_file, request

from goblin_gen.goblin import Goblin

application = Flask(__name__, template_folder=".")

# Matches strings that only contain numbers
just_numbers_regex = re.compile("^[0-9]*$")


def just_numbers(input: str):
    return just_numbers_regex.match(input)


@application.route("/")
def random_goblin():
    if request.args.get("name"):
        seed = request.args.get("name")
    else:
        # Reseed generator
        random.seed()
        seed = random.randint(0, sys.maxsize)

    # This code implies that future requests should still use the / url
    # and not the cached seed
    # https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#3xx_Redirection
    code = 307
    return redirect(f"/{seed}", code=code)


@application.route("/<seed>")
def seeded_goblin(seed):
    goblin = Goblin.random_goblin(seed)
    # If entire seed is numbers then assume a random seed, otherwise a name
    if not just_numbers(seed):
        goblin.name = seed
    return render_template("template.html", goblin=goblin)


@application.route("/style.css")
def get_style():
    return send_file("style.css")


@application.template_filter("render_set")
def render_set(input: set):
    assert isinstance(input, set)
    return ", ".join(input)


if __name__ == "__main__":
    application.run()
