import random
from typing import Optional

from goblin_gen import data, special_abilities, carry_weights, random_util


class Goblin:
    bab = 1
    speed = 30
    size = "S"
    size_bonus = 1
    cm_size_bonus = -1
    base_init = 0
    name: Optional[str] = None

    base_fort = 2
    base_ref = 0
    base_will = 0

    height_inches: int
    age: int
    weight: int
    hair_colour: str
    eyes: str
    gender: str
    alignment: str

    def __init__(self, **kwargs):
        self.hp = kwargs.get("hp", 5)
        self.str = kwargs.get("str", 10)
        self.dex = kwargs.get("dex", 10)
        self.con = kwargs.get("con", 10)
        self.int = kwargs.get("int", 10)
        self.wis = kwargs.get("wis", 10)
        self.cha = kwargs.get("cha", 10)

        self.type = kwargs.get("type")

        self.special_abilities = set()
        for ability in kwargs.get("special_abilities", []):
            self.add_special_ability(ability)

        # Filled in by `complete_stats`
        self.languages = None

    def str_mod(self):
        return Goblin.get_modifier(self.str)

    def dex_mod(self):
        return Goblin.get_modifier(self.dex)

    def con_mod(self):
        return Goblin.get_modifier(self.con)

    def int_mod(self):
        return Goblin.get_modifier(self.int)

    def wis_mod(self):
        return Goblin.get_modifier(self.wis)

    def cha_mod(self):
        return Goblin.get_modifier(self.cha)

    def set_name(self, name: str):
        self.name = name
        return self

    def __repr__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        if not isinstance(other, Goblin):
            return False
        return self.__dict__ == other.__dict__

    def get_hp(self):
        return self.hp + Goblin.get_modifier(self.con)

    def get_init(self):
        return self.base_init + self.dex_mod()

    def get_ac(self):
        return 10 + self.dex_mod() + self.size_bonus

    def get_touch_ac(self):
        return 10 + self.dex_mod() + self.size_bonus

    def get_flat_footed_ac(self):
        return 10 + self.size_bonus

    def get_fort_save(self):
        return self.base_fort + self.con_mod()

    def get_ref_save(self):
        return self.base_ref + self.dex_mod()

    def get_will_save(self):
        return self.base_will + self.wis_mod()

    def get_bab(self):
        return self.bab

    def get_cmb(self):
        return self.bab + self.str_mod() + self.cm_size_bonus

    def get_cmd(self):
        return 10 + self.bab + self.str_mod() + self.dex_mod() + self.cm_size_bonus

    def get_light_load(self):
        return carry_weights.SmallCharCarryWeightCalc.get_light_carry_weight(self.str)

    def get_medium_load(self):
        return carry_weights.SmallCharCarryWeightCalc.get_med_carry_weight(self.str)

    def get_heavy_load(self):
        return carry_weights.SmallCharCarryWeightCalc.get_max_carry_weight(self.str)

    def format_height(self):
        feet = self.height_inches // 12
        inches = self.height_inches % 12
        return f"{feet}ft {inches}in"

    def add_special_ability(self, abilities):
        if not isinstance(abilities, set):
            try:
                abilities = set(abilities)
            except TypeError:
                abilities = {abilities}

        for ability in abilities - self.special_abilities:
            self.special_abilities.add(ability)
            if ability.apply:
                ability.apply(self)

        return self

    def remove_special_ability(self, ability: special_abilities.SpecialAbility):
        self.special_abilities = {
            s for s in self.special_abilities if s != ability
        }
        return self

    @staticmethod
    def get_modifier(skill: int) -> int:
        return (skill - 10) // 2

    class Gen:
        @staticmethod
        def languages(int: int) -> set:
            int_mod = Goblin.get_modifier(int)

            languages = {data.languages.GOBLIN}
            while len(languages) < int_mod + 1:
                languages.add(data.languages.get_weighted_random())
            return languages

        @staticmethod
        def age() -> int:
            return random.randint(13, 24)

        @staticmethod
        def gender() -> str:
            return random.choice(["Male", "Female"])

        @staticmethod
        def height(gender=None) -> int:
            if gender == "Male":
                return random.randint(34, 40)
            elif gender == "Female":
                return random.randint(32, 38)
            else:
                return random.randint(33, 39)

        @staticmethod
        def weight(gender=None) -> int:
            if gender == "Male":
                return random.randint(32, 38)
            elif gender == "Female":
                return random.randint(27, 33)
            else:
                return random.randint(29, 35)

    @staticmethod
    def random_goblin(seed):
        random.seed(seed)
        # Choice from list of functions which is then executed
        goblin = random.choice([
            GoblinTypes.normal_goblin,
            GoblinTypes.fat_goblin,
            GoblinTypes.smart_goblin,
            GoblinTypes.thorny_goblin
        ])()
        goblin.complete_stats()
        return goblin

    def complete_stats(self):
        """Add any stats that are common to all goblins and calculated from base stats"""
        self.languages = Goblin.Gen.languages(self.int)
        self.age = Goblin.Gen.age()
        self.gender = Goblin.Gen.gender()
        self.height_inches = Goblin.Gen.height(self.gender)
        self.weight = Goblin.Gen.weight(self.gender)
        self.hair_colour = data.hair.get_random()
        self.eyes = data.eyes.get_random()

        self.alignment = data.alignments.get_weighted_random()
        return self


class GoblinTypes:
    @staticmethod
    def normal_goblin():
        """Run of the mill goblin, slight randomisation
        avg = 10.5
        """
        return Goblin(
            str=random_util.get_int(11),
            dex=random_util.get_int(15),
            con=random_util.get_int(12),
            int=random_util.get_int(10),
            wis=random_util.get_int(9),
            cha=random_util.get_int(6),
            type="normal",
        ).add_special_ability([
            special_abilities.dark_vision, special_abilities.skilled, special_abilities.improved_initiative
        ])

    @staticmethod
    def fat_goblin():
        """Strong, tough, slow and dumb
        avg = 10
        """
        return Goblin(
            str=random_util.get_int(14),
            dex=random_util.get_int(12),
            con=random_util.get_int(15),
            int=random_util.get_int(7),
            wis=random_util.get_int(6),
            cha=random_util.get_int(6),
            type="fat",
        ).add_special_ability([
            special_abilities.dark_vision, special_abilities.skilled, special_abilities.improved_initiative
        ])

    @staticmethod
    def smart_goblin():
        """Smarter, but fragile and less atheletic
        avg = 10
        """
        return Goblin(
            str=random_util.get_int(9),
            dex=random_util.get_int(12),
            con=random_util.get_int(8),
            int=random_util.get_int(12),
            wis=random_util.get_int(11),
            cha=random_util.get_int(8),
            type="smart",
        ).add_special_ability([
            special_abilities.dark_vision, special_abilities.skilled, special_abilities.improved_initiative
        ])

    @staticmethod
    def thorny_goblin():
        """Coated in thorns, lower dex
        avg = 10.16
        """
        goblin = GoblinTypes.normal_goblin()
        goblin.dex = goblin.dex - 2
        goblin.type = "thorny"
        goblin.add_special_ability(special_abilities.thorny)

        return goblin
