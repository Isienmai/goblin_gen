class SpecialAbility:
    def __init__(self, name: str, description: str, apply=None):
        """
        :param name: name of ability
        :param description: ability description
        :param apply: function to modify goblin to apply special ability
        """
        self.name = name
        self.description = description
        self.apply = apply

    def __repr__(self):
        return self.name


dark_vision = SpecialAbility(
    "Dark Vision", "See in the dark for 60ft"
)
light_blindness = SpecialAbility(
    "Light Blindness", "Blind for 1 round if exposed to bright light. Dazzled while in bright light"
)
thorny = SpecialAbility(
    "Thorny",
    """Spikey goblin that gains a slam attack dealing 1d3 damage plus grab. 
    Deals 1d3 damage to any creature in grapple, or any attacker using unarmed
     or natural attacks. -2 Dex""",
    lambda g: modify_variable(g, "dex", -2)
)
skilled = SpecialAbility(
    "Skilled",
    "+4 to ride and stealth"
    # TODO add skills
)
improved_initiative = SpecialAbility(
    "Improved Initiative",
    "+4 to initiative",
    lambda g: modify_variable(g, "base_init", +4)
)

"""basic set of special abilities that all goblins have"""
basic = {
    dark_vision, skilled, improved_initiative
}


def modify_variable(goblin, name, amount):
    value = getattr(goblin, name)
    setattr(goblin, name, value + amount)

